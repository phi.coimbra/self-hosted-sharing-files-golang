package main

import (
	"time"
    "strings"

	"github.com/dustin/go-humanize"
)

var defaultExpiryList = []uint64{
	86400,
	259200,
	604800,
	31536000,
}

var tempo = [7]string{"horas", "dias", "dias", "ano"}

type ExpirationTime struct {
	Seconds uint64
	Human   string
}

var neverExpire = time.Unix(0, 0)

// Determine if a file with expiry set to "ts" has expired yet
func isTsExpired(ts time.Time) bool {
	now := time.Now()
	return ts != neverExpire && now.After(ts)
}

// Determine if the given filename is expired
func isFileExpired(filename string) (bool, error) {
	metadata, err := metadataRead(filename)
	if err != nil {
		return false, err
	}

	return isTsExpired(metadata.Expiry), nil
}

// Return a list of expiration times and their humanized versions
func listExpirationTimes() []ExpirationTime {
	epoch := time.Now()
	actualExpiryInList := false
	var expiryList []ExpirationTime

	cont := 0

	for _, expiry := range defaultExpiryList {
		if Config.maxExpiry == 0 || expiry <= Config.maxExpiry {
			if expiry == Config.maxExpiry {
				actualExpiryInList = true
			}

			duration := time.Duration(expiry) * time.Second
			t := humanize.RelTime(epoch, epoch.Add(duration), "", "")
			i := strings.Index(t, " ")
			//t := strconv.ParseUint(duration, 10, 64)
			expiryList = append(expiryList, ExpirationTime{
				expiry,
				t[0:i] + " " + tempo[cont],
				//humanize.RelTime(epoch, epoch.Add(duration), "", ""),
			})
		}
		cont++
	}

	expiryList = append(expiryList, ExpirationTime{
		0,
		"Nunca",
	})

	if Config.maxExpiry == 0 {
		expiryList = append(expiryList, ExpirationTime{
			2419200,
			"1 Mês",
		})
	} else if actualExpiryInList == false {
		duration := time.Duration(Config.maxExpiry) * time.Second
		expiryList = append(expiryList, ExpirationTime{
			Config.maxExpiry,
			humanize.RelTime(epoch, epoch.Add(duration), "", ""),
		})
	}

	return expiryList
}
