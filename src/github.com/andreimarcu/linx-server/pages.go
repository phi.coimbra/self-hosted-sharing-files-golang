package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"net/url"

	"io/ioutil"


	"gopkg.in/gomail.v2"
	"github.com/flosch/pongo2"
	"github.com/zenazn/goji/web"

)

type RespType int

const (
	RespPLAIN RespType = iota
	RespJSON
	RespHTML
	RespAUTO
)

var parts []string

func indexHandler(c web.C, w http.ResponseWriter, r *http.Request) {
	err := renderTemplate(Templates["index.html"], pongo2.Context{
		"maxsize":    Config.maxSize,
		"expirylist": listExpirationTimes(),
	}, r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func pasteHandler(c web.C, w http.ResponseWriter, r *http.Request) {
	err := renderTemplate(Templates["paste.html"], pongo2.Context{
		"expirylist": listExpirationTimes(),
	}, r, w)
	if err != nil {
		oopsHandler(c, w, r, RespHTML, "")
	}
}

var emails []string

func sendlinkHandler(c web.C, w http.ResponseWriter, r *http.Request) {
	parts = strings.Split(r.RequestURI, "=")
	err := renderTemplate(Templates["sendmail.html"], pongo2.Context{
		"email2": strings.Split(parts[4], "&")[0],
		"email1": strings.Split(parts[2], "&")[0],
		"link": strings.Split(parts[3], "&")[0],
	}, r, w)

	emails = strings.Split(strings.Split(parts[2], "&")[0], ",")

	//fmt.Println("Scheme: " + r.RequestURI )

	//fmt.Println("Emails: " + emails)

	fmt.Println("Envio: " + strings.Split(parts[3], "/")[0]+"//"+strings.Split(parts[3], "/")[2]+"/selif/"+strings.Split(strings.Split(parts[3], "&")[0], "/")[3])

	//enviaMail()
	mail()

	if err != nil {
		oopsHandler(c, w, r, RespHTML, "")
	}
}

func driversHandler(c web.C, w http.ResponseWriter, r *http.Request) {

	files()

	parts = strings.Split(r.RequestURI, "=")
	err := renderTemplate(Templates["drivers.html"], pongo2.Context{
		"email1": "teste1",
		"email2": "teste2",
		"link": "teste3",
		"files": dataNice,
	}, r, w)

	fmt.Println("Scheme: " + r.RequestURI )


	fmt.Println("ACESSO DRIVERS")

	//enviaMail()
	//mail()

	if err != nil {
		oopsHandler(c, w, r, RespHTML, "")
	}
}

func hello(c web.C, w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, %s!", c.URLParams["envia"])
}

func apiDocHandler(c web.C, w http.ResponseWriter, r *http.Request) {
	err := renderTemplate(Templates["API.html"], pongo2.Context{
		"siteurl": getSiteURL(r),
	}, r, w)
	if err != nil {
		oopsHandler(c, w, r, RespHTML, "")
	}
}

func notFoundHandler(c web.C, w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(404)
	err := renderTemplate(Templates["404.html"], pongo2.Context{}, r, w)
	if err != nil {
		oopsHandler(c, w, r, RespHTML, "")
	}
}

func oopsHandler(c web.C, w http.ResponseWriter, r *http.Request, rt RespType, msg string) {
	if msg == "" {
		msg = "Oops! Something went wrong..."
	}

	if rt == RespHTML {
		w.WriteHeader(500)
		renderTemplate(Templates["oops.html"], pongo2.Context{"msg": msg}, r, w)
		return

	} else if rt == RespPLAIN {
		w.WriteHeader(500)
		fmt.Fprintf(w, "%s", msg)
		return

	} else if rt == RespJSON {
		js, _ := json.Marshal(map[string]string{
			"error": msg,
		})

		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(500)
		w.Write(js)
		return

	} else if rt == RespAUTO {
		if strings.EqualFold("application/json", r.Header.Get("Accept")) {
			oopsHandler(c, w, r, RespJSON, msg)
		} else {
			oopsHandler(c, w, r, RespHTML, msg)
		}
	}
}

func badRequestHandler(c web.C, w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusBadRequest)
	err := renderTemplate(Templates["400.html"], pongo2.Context{}, r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func unauthorizedHandler(c web.C, w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(401)
	err := renderTemplate(Templates["401.html"], pongo2.Context{}, r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}



func mail() {
	var nome = ""
	var newstr = ""
	var link = strings.Split(parts[3], "&")[0]
	err := strings.Split(parts[4], "&")[0]
	m := gomail.NewMessage()
	m.SetAddressHeader("From", "evento@antidotodesign.com.br", "FileTransfer - Antidoto")

	//fmt.Println("Emails: " + emails)

	//m.SetHeader("To", strings.Split(parts[2], "&")[0])
	//emails := string("\"") + strings.Replace(strings.Split(parts[2], "&")[0], ";", "\",\"", -1)+ string("\"")

	//a := [...]string{"phillipe@antidotodesign.com.br", "phi.coimbra@gmail.com"}



//	values := []string{"\"To\""}
	//slice := append(values, emails)

	//result1 := strings.Join(slice, ",")

	//h := map[string][]string{"To": {"phillipe@antidotodesign.com.br", "phi.coimbra@gmail.com"}}
//	b := map[string][]string{"To": {"<foo@bar.mail>"}}
	//m.SetAddressHeader("To", "phillipe@antidotodesign.com.br", "Phillipe - Antidoto")
	//m.SetAddressHeader("To", "macabro23@hotmail.com", "Phillipe - GMail")
	//gomail.SetHeader(h)

	//fmt.Println("Emails: " + result1)

	//m.SetHeader("To", emails)

	switch size_mail := len(emails); size_mail {
		case 1:
			m.SetHeader("To", emails[0])
		case 2:
			m.SetHeader("To", emails[0], emails[1])
		case 3:
			m.SetHeader("To", emails[0], emails[1], emails[2])
		case 4:
			m.SetHeader("To", emails[0], emails[1], emails[2], emails[3], emails[4])
		case 5:
			m.SetHeader("To", emails[0], emails[1], emails[2], emails[3], emails[4], emails[5])
	}

	if err != "" {
		nome = strings.Split(err, "@")[0]
		newstr,_ = url.QueryUnescape(parts[5])
		m.SetHeader("Bcc", strings.Split(parts[4], "&")[0])
	}
	m.SetHeader("Subject", "Arquivo da Antidoto")
	//m.Attach("/home/Alex/lolcat.jpg")

	m.SetBody("text/html", "<body bgcolor=\"#f7f7f7\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100% !important;margin:0 !important;height:100%;color:#676767;\"> <table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container-for-gmail-android\" width=\"100%\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;border-collapse:collapse !important;min-width:600px;\"><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td align=\"left\" valign=\"top\" width=\"100%\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;\"> <center style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"> <img src=\"http://www.antidotodesign.com.br/images/email/SBb2fQPrQ5ezxmqUTgCr_transparent.png\" class=\"force-width-gmail\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;min-width:600px;height:0px !important;line-height:1px !important;font-size:1px !important;\"><table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" bgcolor=\"#ffffff\" background=\"http://www.antidotodesign.com.br/images/email/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" style=\"background-color:transparent;font-family:'Helvetica Neue', 'Verdana' !important;border-collapse:collapse !important;\"><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td width=\"100%\" height=\"80\" valign=\"top\" style=\"text-align:center;vertical-align:middle;font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;line-height:21px;border-collapse:collapse;\"> <center style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"> <table cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"w320\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;border-collapse:collapse !important;width:580px !important;\"><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td class=\"pull-left mobile-header-padding-left\" style=\"vertical-align:middle;font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:left;line-height:21px;border-collapse:collapse;width:290px;padding-left:10px;\"> <a href=\"http://www.antidotodesign.com.br/\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;color:#12508a;text-decoration:none !important;\"><img width=\"137\" height=\"47\" src=\"http://www.antidotodesign.com.br/images/email/logo.jpg\" alt=\"logo\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;border:none;\"></a> </td> <td class=\"pull-right mobile-header-padding-right\" style=\"color:#777777;font-family:Helvetica, Arial, sans-serif;font-size:14px;text-align:right;line-height:21px;border-collapse:collapse;width:290px;padding-left:10px;\"> <a href=\"https://www.facebook.com/antidotodesign/\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;color:#12508a;text-decoration:none !important;\"><img width=\"38\" height=\"47\" src=\"http://www.antidotodesign.com.br/images/email/LMPMj7JSRoCWypAvzaN3_social_09.jpg\" alt=\"facebook\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;border:none;\"></a> </td> </tr></table></center> </td> </tr></table></center> </td> </tr><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color:#f7f7f7;font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;padding:20px 0 30px;\" class=\"content-padding\"> <center style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"> <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;border-collapse:collapse !important;width:320px !important;\"><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td class=\"header-lg\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#4d4d4d;text-align:center;line-height:normal;border-collapse:collapse;font-weight:700;padding:35px 0 0;\"> Tem um arquivo da Antidoto para você! </td> </tr><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td class=\"free-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;width:100% !important;padding:10px 60px 0px;\"> Arquivo compartilhado </td> </tr><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td class=\"mini-block-container\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;padding:30px 50px;width:500px;\"> <table cellspacing=\"0\" cellpadding=\"0\" width=\"570px\" style=\"border-collapse:collapse !important;font-family:'Helvetica Neue', 'Verdana' !important;\"><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td class=\"mini-block\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;background-color:#ffffff;width:498px;border:1px solid #cccccc;border-radius:5px;padding:60px 75px;\"> <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;border-collapse:collapse !important;\"><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td style=\"padding-bottom:30px;font-size:16px;font-family:Helvetica, Arial, sans-serif;color:#777777;text-align:left;line-height:21px;border-collapse:collapse;\"><span style=\"font-weight:900;color:#12508a;font-size:18px;font-family:'Helvetica Neue', 'Verdana' !important;\"> "+nome+" </span> <span style=\"font-family:'Helvetica Neue', 'Verdana'!important;\"> enviou-lhe um arquivo com a seguinte descrição:<span style=\"font-weight:900;color:#102f55;font-size:18px;text-align:center;font-family:'Helvetica Neue', 'Verdana' !important;\"></span></span><p style=\"font-weight:900;font-size:15px;\" >"+newstr+"</p><p style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"> Você pode acessar o arquivo no link abaixo e nele você terá acesso ao link direto ou poderá baixá-lo por torrent.</p> </td> </tr><tr style=\"background-color:#000000;font-family:'Helvetica Neue', 'Verdana' !important;\"><td class=\"button\" style=\"background-color:#000000;font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;padding:5px 5px 5px;\"> <div style=\"font-family:'Helvetica Neue', 'Verdana' !important; \"><a class=\"button-mobile\" href=\""+link+"\" style=\"background-color:#000000;border-radius:5px;color:#ffffff;display:inline-block;font-family:'Helvetica Neue', 'Verdana' !important;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none !important;width:255px;-webkit-text-size-adjust:none;\">ACESSAR O ARQUIVO</a></div> </td> </tr></table></td> </tr></table></td> </tr></table></center> </td> </tr><td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color:#ffffff;border-top:1px solid #e5e5e5;border-bottom:1px solid #e5e5e5;font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;\"> <center style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"> <table cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"w320\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;border-collapse:collapse !important;width:320px !important;\"><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td class=\"content-padding\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;padding:20px 0 30px;\"> <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;border-collapse:collapse !important;\"><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td class=\"header-md\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:24px;color:#4d4d4d;text-align:center;line-height:normal;border-collapse:collapse;font-weight:700;padding:35px 0 0;\"> Nossos ultimos trabalhos </td> </tr></table></td> </tr><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td style=\"padding-bottom:75px;font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;\"> <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse !important;font-family:'Helvetica Neue', 'Verdana' !important;\"><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td class=\"info-block\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;padding:0 20px;width:260px;\"> <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse !important;font-family:'Helvetica Neue', 'Verdana' !important;\"><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td class=\"block-rounded\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;border-radius:5px;border:1px solid #e5e5e5;vertical-align:top;width:260px;\"> <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;border-collapse:collapse !important;\"><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td class=\"info-img\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;width:258px;border-radius:5px 5px 0 0;\"> <a href=\"\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;color:#ffffff;text-decoration:none !important;\"><img class=\"info-img\" src=\"http://www.antidotodesign.com.br/images/email/raiox.jpg\" alt=\"img\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;border:none;width:258px;border-radius:5px 5px 0 0;\"></a> </td> </tr><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td style=\"padding:15px;font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;\"> <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;border-collapse:collapse !important;\"><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td style=\"text-align:center;width:155px;font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;line-height:21px;border-collapse:collapse;\"> <a href=\"\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;color:#ffffff;text-decoration:none !important;\"><span class=\"header-sm\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;font-size:18px;font-weight:700;line-height:1.3;padding:5px 0;color:#4d4d4d;\">RAIO-X DO BRASIL</span></a><br style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"> Um verdadeiro golaaaaço da Antídoto! Conheça o Raio-X interativo do Brasil instalado durante a Copa no Tietê Plaza Shopping. </td> </tr></table></td> </tr><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td style=\"background-color:#000000;padding:15px;font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;\"> <div style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><a class=\"button-width\" href=\"http://antidotodesign.com.br/portfolio/raio-x-do-brasil\" style=\"background-color:#000000;border-radius:5px;color:#ffffff;display:inline-block;font-family:'Helvetica Neue', 'Verdana' !important;font-size:14px;font-weight:regular;line-height:15px;text-align:center;text-decoration:none !important;-webkit-text-size-adjust:none;width:228px;\">SAIBA MAIS</a></div> </td> </tr></table></td> </tr></table></td> <td class=\"info-block\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;padding:0 20px;width:260px;\"> <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse !important;font-family:'Helvetica Neue', 'Verdana' !important;\"><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td class=\"block-rounded\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;border-radius:5px;border:1px solid #e5e5e5;vertical-align:top;width:260px;\"> <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;border-collapse:collapse !important;\"><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td class=\"info-img\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;width:258px;border-radius:5px 5px 0 0;\"> <a href=\"\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;color:#ffffff;text-decoration:none !important;\"><img width=\"258\" height=\"210\" class=\"info-img\" src=\"http://www.antidotodesign.com.br/images/email/salesforce.jpg\" alt=\"img\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;border:none;width:258px;border-radius:5px 5px 0 0;\"></a> </td> </tr><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td style=\"padding:15px;font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;\"> <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;border-collapse:collapse !important;\"><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td style=\"text-align:center;width:155px;font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;line-height:21px;border-collapse:collapse;\"> <a href=\"\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;color:#ffffff;text-decoration:none !important;\"><span class=\"header-sm\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;font-size:18px;font-weight:700;line-height:1.3;padding:5px 0;color:#4d4d4d;\">SALESFORCE</span></a><br style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"> A Antídoto em parceria com a Beats Brasil, desenvolveu diversas soluções interativas para o Salesforce World Tour 2018. Confira! </td> </tr></table></td> </tr><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td style=\"background-color:#000000;padding:15px;font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;\"> <div style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><a class=\"button-width\" href=\"http://antidotodesign.com.br/portfolio/diversao-interativa\" style=\"background-color:#000000;border-radius:5px;color:#ffffff;display:inline-block;font-family:'Helvetica Neue', 'Verdana' !important;font-size:14px;font-weight:regular;line-height:15px;text-align:center;text-decoration:none !important;-webkit-text-size-adjust:none;width:228px;\">SAIBA MAIS</a></div> </td> </tr></table></td> </tr></table></td> </tr></table></td> </tr></table></center> </td> <tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color:#f7f7f7;height:100px;font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;\"> <center style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"> <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\" style=\"font-family:'Helvetica Neue', 'Verdana' !important;border-collapse:collapse !important;width:320px !important;\"><tr style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><td style=\"padding:25px 0 25px;font-family:Helvetica, Arial, sans-serif;font-size:14px;color:#777777;text-align:center;line-height:21px;border-collapse:collapse;\"> <strong style=\"font-family:'Helvetica Neue', 'Verdana' !important;\">Antidoto Design e Tecnologia</strong><br style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"> Junta Mizumoto 296<br style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"> São Paulo <br style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"><br style=\"font-family:'Helvetica Neue', 'Verdana' !important;\"></td> </tr></table></center> </td> </tr></table></body></html>")

	d := gomail.NewDialer("smtp.gmail.com", 587, "evento@antidotodesign.com.br", "antidoto1234")

	// Send the email to Bob, Cora and Dan.
	if err := d.DialAndSend(m); err != nil {
		panic(err)
	}


	//if err := script.Exec("static/php/enviar.php?email=phi.coimbra@gmail.com"); err != nil {


}

var dataNice = ""

func files() {
	b, err := ioutil.ReadFile("meta/files.txt") // just pass the file name
	if err != nil {
		fmt.Print(err)
	}

	//fmt.Println(b) // print the content as 'bytes'

	str := string(b) // convert content to a 'string'

	//fmt.Println(str) // print the content as a 'string'

	dataNice = str
}

