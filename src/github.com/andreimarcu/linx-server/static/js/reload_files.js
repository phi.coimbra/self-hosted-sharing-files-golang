/**
 * Created by Phillipe on 19/10/2016.
 */

$( function() {

	$(".search-alert-3").hide();
	
	function testAjax() {

		var url = $("#basic-addon1").attr("rel");

		var utfstring = unescape(encodeURIComponent(url));

		var dataurl = JSON.parse(utfstring);
	   
	   $.widget( "custom.catcomplete", $.ui.autocomplete, {
        _create: function() {
            this._super();
            this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
        },
        _renderMenu: function( ul, items ) {
            var that = this,
                currentCategory = "";
            $.each( items, function( index, item ) {
                var li;
                if ( item.category != currentCategory ) {
                    ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
                    currentCategory = item.category;
                }
                li = that._renderItemData( ul, item );
                if ( item.category ) {
                    li.attr( "aria-label", item.category + " : " + item.label );
                }
            });
        }
    });

	   $( "#search" ).catcomplete({
        delay: 0,
        source: dataurl.arquivos,
        select: function( event, ui ) {
            log( ui.item ? ui.item.link :
            "Nada foi escolhido : " + this.value );
            $(".search-alert-3").show();
        }
    });

	};
	
	function log( message ) {
        //$( "<div/>" ).text( message ).prependTo( "#log" );
        $("#alert-id-3").html(" " + message);
        $("#alert-id-3").attr( "href", message );
       // $( "#log" ).attr( "scrollTop", 0 );
    }
		
	testAjax();	


} );
