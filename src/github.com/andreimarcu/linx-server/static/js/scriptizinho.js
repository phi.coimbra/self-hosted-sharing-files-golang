/**
 * Created by Phillipe on 19/10/2016.
 */

$( function() {
	
	var dialog, form,
 
      // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
      emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
      password = $( "#password" ),
      allFields = $( [] ).add( password ),
      tips = $( ".validateTips" );
	  
 
    function updateTips( t ) {
     // alert( t )
        //.addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 1500 );
      }, 500 );
    }
 
    function checkLength( o, n, min, max ) {
      if ( o.val().length > max || o.val().length < min ) {
        o.addClass( "ui-state-error" );
        updateTips( "Length of " + n + " must be between " +
          min + " and " + max + "." );
        return false;
      } else {
        return true;
      }
    }
 
    function checkRegexp( o, regexp, n ) {
      if ( o.val() !== 'virus' ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
 
    function addUser() {
      var valid = true;
      allFields.removeClass( "ui-state-error" );

      valid = valid && checkLength( password, "password", 5, 16 );

      valid = valid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Senha incorreta" );
 
      if ( valid ) {
        $( "#users tbody" ).append( "<tr>" +
         "<td>" + password.val() + "</td>" +
        "</tr>" );
        dialog.dialog( "close" );
		$("#fileupload, #container").unwrap('<div class="blur-all">');
      }
      return valid;
    }
 
    dialog = $( "#dialog-form" ).dialog({
	  position: { 
	  my: "center top+150", 
	  at: "center top", 
	  of: window },
		
      autoOpen: false,
      height: 220,
      width: 350,
      modal: true,
	  show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "explode",
        duration: 1000
      },
	  open: function(event, ui) {
        $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
		//window.setTimeout (function(){ 
			//$(this).select(); 
		//},2000);
		//$(this).focus();
		//$("#fileupload").wrap('<div class="blur-all">').append($( "#dialog-form" ));
	 },
      buttons: {
        "Desbloquear": addUser,
      },
      close: function() {
        form[ 0 ].reset();
        allFields.removeClass( "ui-state-error" );
      }
    });
	
	$('input').on('focus', function (e) {
		$(this)
			.one('mouseup', function () {
				$(this).select();
				return false;
			})
			.select();
	});

 
    form = dialog.find( "form" ).on( "submit", function( event ) {
      event.preventDefault();
      addUser();
    });
 
    

    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();

    $( "#expires" ).selectmenu();
    //$( "input" ).checkboxradio();
    $( "#progressbarmail" ).progressbar({
        value: false
    });


   /* $(document).ready(function() {
        $('#popinfo').popover('show');
    });*/

    setTimeout(function(){
        //$('#popinfo').popover('show');
		$("#fileupload, #container").wrap('<div class="blur-all">');
		dialog.dialog( "open" );
		
    }, 100);

    setTimeout(function(){
        $('#popinfo').popover('destroy');
    }, 5000);


    $( "#menu" ).menu({
        items: "> :not(.ui-widget-header)"
    });
    $( "#menu2" ).menu({
        items: "> :not(.ui-widget-header)"
    });

    $( "#accordion" ).accordion({
        active: false,
        collapsible: true
    });



    function runEffect() {
        // get effect type from
        var selectedEffect =  "blind";

        // Most effect types need no options passed by default
        var options = {};
        // some effects have required parameters
        if ( selectedEffect === "scale" ) {
            options = { percent: 50 };
        } else if ( selectedEffect === "size" ) {
            options = { to: { width: 280, height: 185 } };
        }

        // Run the effect
        $( "#effect" ).show( selectedEffect, options, 500, callback );
    };

    //callback function to bring a hidden box back
    function callback() {
        setTimeout(function() {
            var options = {};
            $( "#effect" ).hide( "blind", options, 1000, callback2 );
        }, 8000 );

    };

    function callback2() {}

    // Set effect from select menu value
    $( "#button" ).on( "click", function() {
        runEffect();
    });


    $( "#effect" ).hide();

    setTimeout(runEffect , 1000 );



   
  /*  $(".search-alert-1").hide();
    $(".search-alert-2").hide();

    $(".ui-delete-button").hide();*/
    $(".ui-response").hide();
    $(".search-alert-3").hide();
	$(".info-alert-1").hide();
	$("#randomize").hide();
	$(".btn-subir-outro").hide();

    function log( message ) {
        //$( "<div/>" ).text( message ).prependTo( "#log" );
        $("#alert-id-3").html(" " + message);
        $("#alert-id-3").attr( "href", message );
       // $( "#log" ).attr( "scrollTop", 0 );
    }

   // $(".form-control").attr( "placeholder", 'Destinatário do link' );

    $( "#button-envia-mail" ).on( "click", function() {
		if($("#field-email-2").val() != "" && $("#field-email-1").val() != "" && $("#field-info").val() != ""){
			var url = $("#button-envia-mail").attr("rel");
			
			$(".progress-bar-striped").attr( "style", "width: 100%" );
			$(".progress-bar-striped").html( "Enviando..." );
			var emails = [];
			for(var i=2; i<7; i++){
				if($("#field-email-"+i).val() != ''){
					emails.push($("#field-email-"+i).val())
				}
			}
			
			var tudo = "mail="+emails+"&link="+$("#alert-id-1").attr( "href")+ "&mailbcc="+$("#field-email-1").val()+ "&info="+$("#field-info").val();
			
			window.location = url + tudo;

		}else{
			$( "#dialog" ).dialog();
		}

    });



} );
