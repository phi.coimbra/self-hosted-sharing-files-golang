<?php

$email = $_GET['email'];
$email2 = $_GET['email2'];
$link = $_GET['link'];

//Validação de e-mail

if ( !filter_var($email, FILTER_VALIDATE_EMAIL) ){
	die("Endereço de e-mail inválido");
}

require 'mailer/PHPMailerAutoload.php';

$mail = new PHPMailer();
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Debugoutput = 'html';
$mail->Host = 'mail.antidotodesign.com.br';
$mail->Port = 25;
//$mail->SMTPSecure = 'tls'; //ativar no online

$mail->SMTPAuth = true;

$mail->Username = "eventos@antidotodesign.com.br";
$mail->Password = "virus2010Antidoto";

$mail->setFrom('bruno@antidotodesign.com.br', utf8_decode('Arquivo Antidoto'));
$mail->addAddress($email);

$mail->Subject = utf8_decode('EcoFolder Biomarins');

$avArray = array();
$avArray[0] = "Estudo_Acompanhamento.pdf";
$avArray[1] = "Re-survey.pdf";
$avArray[2] = "Assessment_of_the_impact_of_phenylketonuria.pdf";
$avArray[3] = "Phenylalanine_Hydrosylase_Deficiency_Practice_Guideline_AOP_Jan_2013.pdf";
$avArray[4] = "Harmatz_The_MorquioA.pdf";
$avArray[5] = "MorquioA_ManagementGuidelines.pdf";


$nomesArray = array();
$nomesArray[0] = "Naglazyme - Estudo Acompanhamento";
$nomesArray[1] = "Naglazyme - Longitudinal Natural History";
$nomesArray[2] = "Kuvan - Assessment of the impact of phenylketonuria";
$nomesArray[3] = "Kuvan - Phenylalanine Hydrosylase Deficiency Practice Guideline AOP Jan 2013";
$nomesArray[4] = "Vimizim - Harmatz The Morquio A";
$nomesArray[5] = "Vimizim - Morquio A Management Guidelines";

$escolhidosStr = $_GET['pdf'];
$escolhidosArray = preg_split("/,/", $escolhidosStr, -1, PREG_SPLIT_NO_EMPTY);
$escolhidosURL = "";

foreach($escolhidosArray as $escolhido){
	$escolhidosURL .= "<a href='http://dev.antidotodesign.com.br/biomarin/pdf/" . $avArray[$escolhido] . "' style='color:#00aeef; text-decoration:none'>" .  utf8_decode($nomesArray[$escolhido]) . "</a><br />";
}

$msg = '<style type="text/css">
    /* Take care of image borders and formatting, client hacks */
    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
    a img { border: none; }
    table { border-collapse: collapse !important;}
    #outlook a { padding:0; }
    .ReadMsgBody { width: 100%; }
    .ExternalClass { width: 100%; }
    .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }
    table td { border-collapse: collapse; }
    .ExternalClass * { line-height: 115%; }
    .container-for-gmail-android { min-width: 600px; }


    /* General styling */
    * {
        font-family: Helvetica, Arial, sans-serif;
    }

    body {
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: none;
        width: 100% !important;
        margin: 0 !important;
        height: 100%;
        color: #676767;
    }

    td {
        font-family: Helvetica, Arial, sans-serif;
        font-size: 14px;
        color: #777777;
        text-align: center;
        line-height: 21px;
    }

    a {
        color: #676767;
        text-decoration: none !important;
    }

    .pull-left {
        text-align: left;
    }

    .pull-right {
        text-align: right;
    }

    .header-lg,
    .header-md,
    .header-sm {
        font-size: 32px;
        font-weight: 700;
        line-height: normal;
        padding: 35px 0 0;
        color: #4d4d4d;
    }

    .header-md {
        font-size: 24px;
    }

    .header-sm {
        padding: 5px 0;
        font-size: 18px;
        line-height: 1.3;
    }

    .content-padding {
        padding: 20px 0 30px;
    }

    .mobile-header-padding-right {
        width: 290px;
        text-align: right;
        padding-left: 10px;
    }

    .mobile-header-padding-left {
        width: 290px;
        text-align: left;
        padding-left: 10px;
    }

    .free-text {
        width: 100% !important;
        padding: 10px 60px 0px;
    }

    .block-rounded {
        border-radius: 5px;
        border: 1px solid #e5e5e5;
        vertical-align: top;
    }

    .button {
        padding: 5px 0 0;
    }

    .info-block {
        padding: 0 20px;
        width: 260px;
    }

    .mini-block-container {
        padding: 30px 50px;
        width: 500px;
    }

    .mini-block {
        background-color: #ffffff;
        width: 498px;
        border: 1px solid #cccccc;
        border-radius: 5px;
        padding: 60px 75px;
    }

    .block-rounded {
        width: 260px;
    }

    .info-img {
        width: 258px;
        border-radius: 5px 5px 0 0;
    }

    .force-width-img {
        width: 480px;
        height: 1px !important;
    }

    .force-width-full {
        width: 600px;
        height: 1px !important;
    }

    .user-img img {
        width: 82px;
        border-radius: 5px;
        border: 1px solid #cccccc;
    }

    .user-img {
        width: 92px;
        text-align: left;
    }

    .user-msg {
        width: 236px;
        font-size: 14px;
        text-align: left;
        font-style: italic;
    }

    .code-block {
        padding: 10px 0;
        border: 1px solid #cccccc;
        color: #4d4d4d;
        font-weight: bold;
        font-size: 18px;
        text-align: center;
    }

    .force-width-gmail {
        min-width:600px;
        height: 0px !important;
        line-height: 1px !important;
        font-size: 1px !important;
    }

    .button-width {
        width: 228px;
    }

</style>

<style type="text/css" media="screen">
    @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);
</style>

<style type="text/css" media="screen">
    @media screen {
        /* Thanks Outlook 2013! */
        * {
            font-family: \'Oxygen\', \'Helvetica Neue\', \'Arial\', \'sans-serif\' !important;
        }
    }
</style>

<style type="text/css" media="only screen and (max-width: 480px)">
    /* Mobile styles */
    @media only screen and (max-width: 480px) {

        table[class*="container-for-gmail-android"] {
            min-width: 290px !important;
            width: 100% !important;
        }

        table[class="w320"] {
            width: 320px !important;
        }

        img[class="force-width-gmail"] {
            display: none !important;
            width: 0 !important;
            height: 0 !important;
        }

        a[class="button-width"],
        a[class="button-mobile"] {
            width: 248px !important;
        }

        td[class*="mobile-header-padding-left"] {
            width: 160px !important;
            padding-left: 0 !important;
        }

        td[class*="mobile-header-padding-right"] {
            width: 160px !important;
            padding-right: 0 !important;
        }

        td[class="header-lg"] {
            font-size: 24px !important;
            padding-bottom: 5px !important;
        }

        td[class="header-md"] {
            font-size: 18px !important;
            padding-bottom: 5px !important;
        }

        td[class="content-padding"] {
            padding: 5px 0 30px !important;
        }

        td[class="button"] {
            padding: 15px 0 5px !important;
        }

        td[class*="free-text"] {
            padding: 10px 18px 30px !important;
        }

        img[class="force-width-img"],
        img[class="force-width-full"] {
            display: none !important;
        }

        td[class="info-block"] {
            display: block !important;
            width: 280px !important;
            padding-bottom: 40px !important;
        }

        td[class="info-img"],
        img[class="info-img"] {
            width: 278px !important;
        }

        td[class="mini-block-container"] {
            padding: 8px 20px !important;
            width: 280px !important;
        }

        td[class="mini-block"] {
            padding: 20px !important;
        }

        td[class="user-img"] {
            display: block !important;
            text-align: center !important;
            width: 100% !important;
            padding-bottom: 10px;
        }

        td[class="user-msg"] {
            display: block !important;
            padding-bottom: 20px !important;
        }
    }
</style>

<table align="center" cellpadding="0" cellspacing="0" class="container-for-gmail-android" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" style="background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;">
            <center>
                <img src="http://s3.amazonaws.com/swu-filepicker/SBb2fQPrQ5ezxmqUTgCr_transparent.png" class="force-width-gmail">
                <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff" background="http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg" style="background-color:transparent">
                    <tr>
                        <td width="100%" height="80" valign="top" style="text-align: center; vertical-align:middle;">
                            <!--[if gte mso 9]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000;height:80px; v-text-anchor:middle;">
                                <v:fill type="tile" src="http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg" color="#ffffff" />
                                <v:textbox inset="0,0,0,0">
                            <![endif]-->
                            <center>
                                <table cellpadding="0" cellspacing="0" width="600" class="w320">
                                    <tr>
                                        <td class="pull-left mobile-header-padding-left" style="vertical-align: middle;">
                                            <a href="http://www.antidotodesign.com.br/"><img width="137" height="47" src="http://www.antidotodesign.com.br/images/email/logo.jpg" alt="logo"></a>
                                        </td>
                                        <td class="pull-right mobile-header-padding-right" style="color: #4d4d4d;">

                                            <a href="https://www.facebook.com/antidotodesign/"><img width="38" height="47" src="http://s3.amazonaws.com/swu-filepicker/LMPMj7JSRoCWypAvzaN3_social_09.gif" alt="facebook" /></a>

                                        </td>
                                    </tr>
                                </table>
                            </center>
                            <!--[if gte mso 9]>
                            </v:textbox>
                            </v:rect>
                            <![endif]-->
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">
            <center>
                <table cellspacing="0" cellpadding="0" width="600" class="w320">
                    <tr>
                        <td class="header-lg">
                           Tem um arquivo da Antidoto para você!
                        </td>
                    </tr>
                    <tr>
                        <td class="free-text">
                           Arquivo compartilhado
                        </td>
                    </tr>
                    <tr>
                        <td class="mini-block-container">
                            <table cellspacing="0" cellpadding="0" width="100%"  style="border-collapse:separate !important;">
                                <tr>
                                    <td class="mini-block">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td style="padding-bottom: 30px;">
                                                    Clique abaixo para acessar a página do <span style="font-weight:700; color: #ff6f6f; font-size: 18px;"><a href=""> arquivo</a></span>.<p> Acessando a página do link, você pode visualizar o conteúdo e se preferir pode baixá-lo por torrent.</p>
                                                </td>
                                            </tr>
                                           <!-- <tr>
                                                <td class="code-block">
                                                    Ax8Sp0023KL2
                                                </td>
                                            </tr>-->
                                            <tr>
                                                <td class="button">
                                                    <div><!--[if mso]>
                                                        <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:155px;" arcsize="15%" strokecolor="#ffffff" fillcolor="#ff6f6f">
                                                            <w:anchorlock/>
                                                            <center style="color:#ffffff;font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;">Download</center>
                                                        </v:roundrect>
                                                        <![endif]--><a class="button-mobile" href="http://"
                                                                       style="background-color:#000000;border-radius:5px;color:#ffffff;display:inline-block;font-family:\'Cabin\', Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:155px;-webkit-text-size-adjust:none;mso-hide:all;">Página do Arquivo</a></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
    <td align="center" valign="top" width="100%" style="background-color: #ffffff;  border-top: 1px solid #e5e5e5; border-bottom: 1px solid #e5e5e5;">
        <center>
            <table cellpadding="0" cellspacing="0" width="600" class="w320">
                <tr>
                    <td class="content-padding">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="header-md">
                                    Nossos ultimos trabalhos!
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding-bottom: 75px;">
                        <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:separate !important;">
                            <tr>
                                <td class="info-block">
                                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:separate !important;">
                                        <tr>
                                            <td class="block-rounded">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="info-img">
                                                            <a href=""><img class="info-img" src="http://www.antidotodesign.com.br/images/email/mesa_dow.jpg" alt="img" /></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding: 15px;">
                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td style="text-align:left; width:155px">
                                                                        <a href=""><span class="header-sm">DOW MESA INTERATIVA</span></a><br />
                                                                        Através da exclusiva mesa de reconhecimento de objetos, os clientes Dow participaram do Quiz interativo de produtos.
                                                                    </td>
                                                                   <!-- <td style="text-align:right; vertical-align: top;">
                                                                        <strong>$29.99</strong>
                                                                    </td> -->
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding: 15px;">
                                                            <div><!--[if mso]>
                                                                <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:228px;" arcsize="15%" strokecolor="#ffffff" fillcolor="#ff6f6f">
                                                                    <w:anchorlock/>
                                                                    <center style="color:#ffffff;font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;">My Account</center>
                                                                </v:roundrect>
                                                                <![endif]--><a class="button-width" href="http://www.antidotodesign.com.br/portfolio/mesa-interativa-dow"
                                                                               style="background-color:#000000;border-radius:5px;color:#ffffff;display:inline-block;font-family:\'Cabin\', Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;-webkit-text-size-adjust:none;mso-hide:all;">Assistir</a></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="info-block">
                                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:separate !important;">
                                        <tr>
                                            <td class="block-rounded">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="info-img">
                                                            <a href=""><img width="258" height="210" class="info-img" src="http://www.antidotodesign.com.br/images/email/sirio_vr.jpg" alt="img" /></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding: 15px;">
                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td style="text-align:left; width:155px">
                                                                        <a href=""><span class="header-sm">SÍRIO LIBANÊS VR</span></a><br />
                                                                        Antidoto desenvolveu uma experiência única e pioneira utilizando os óculos de realidade virtual
                                                                        <!-- Remove bottom br and nbsp when item text longer than one line is -->

                                                                    </td>
                                                                    <!--<td style="text-align:right; vertical-align: top;">
                                                                        <strong>$29.99</strong>
                                                                    </td>-->
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding: 15px;">
                                                            <div><!--[if mso]>
                                                                <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width: 228px;" arcsize="15%" strokecolor="#ffffff" fillcolor="#ff6f6f">
                                                                    <w:anchorlock/>
                                                                    <center style="color:#ffffff;font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;">My Account</center>
                                                                </v:roundrect>
                                                                <![endif]--><a class="button-width" href="http://www.antidotodesign.com.br/portfolio/realidade-virtual-sirio-libanes"
                                                                               style="background-color:#000000;border-radius:5px;color:#ffffff;display:inline-block;font-family:\'Cabin\', Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none; -webkit-text-size-adjust:none;mso-hide:all;">Assistir</a></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </td>
    <tr>
        <td align="center" valign="top" width="100%" style="background-color: #f7f7f7; height: 100px;">
            <center>
                <table cellspacing="0" cellpadding="0" width="600" class="w320">
                    <tr>
                        <td style="padding: 25px 0 25px">
                            <strong>Antidoto Design e Tecnologia</strong><br />
                            Junta Mizumoto 296<br />
                            São Paulo <br /><br />
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>';

$msg .= utf8_decode("<p>Obrigado por visitar nosso estande, segue abaixo os links dos materiais solicitados:</p>"). $escolhidosURL;

$complementoMSG = "<p>Se tiver interesse em outros materiais da Biomarin você pode fazer o download nos links abaixo:</p>";


for ($i = 0; $i < sizeof($escolhidosArray); $i++){
	unset($avArray[$escolhidosArray[$i]]);
}



for ($c = 0; $c < sizeof($nomesArray); $c++){
	if (isset($avArray[$c])){
		$complementoMSG .= "<a href='http://dev.antidotodesign.com.br/biomarin/pdf/" .$avArray[$c] . "' style='color:#00aeef; text-decoration:none'>" . $nomesArray[$c] . "</a><br />";
	}
}

$msg .= utf8_decode($complementoMSG);
$msg .= '<td>
			</td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://dev.antidotodesign.com.br/biomarin/images/bio_05.jpg" width="616" height="210" alt=""></td>
	</tr>
</table>';


$mail->msgHTML($msg);

if (!$mail->send()) {
	echo "Erro ao enviar o e-mail. Por favor tente novamente.";
} else {
	echo "E-mail enviado com sucesso.";
}


function removerPrefixo($txt){
	return substr($txt, 2);
}

?>